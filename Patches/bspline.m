function [ SX, SY, SZ ] = bspline( X, Y, Z, n, m, degre, iterations )
%BEZIER Calcule une surface de Bézier
% X, Y, Z       coordonnées des points de contrôle (matrices n*m)
% n, m          dimensions de la matrice des points de contrôle


% L'algorithme fonctionne en deux étapes, d'abord sur les lignes, puis
% sur les colonnes

SX_tmp = [];
SY_tmp = [];
SZ_tmp = [];

SX = [];
SY = [];
SZ = [];

for i = 1:n
    
    [ sx_i, sy_i, sz_i ] = subdiviser(X(i,:), Y(i,:), Z(i,:), m, degre, iterations);
    
    % Ajout de la courbe
    SX_tmp = [ SX_tmp ; sx_i ];
    SY_tmp = [ SY_tmp ; sy_i ];
    SZ_tmp = [ SZ_tmp ; sz_i ];
    
end

for j = 1:(m*2^iterations)
    
    [ sx_j, sy_j, sz_j ] = subdiviser(SX_tmp(:,j)', SY_tmp(:,j)', SZ_tmp(:,j)', n, degre, iterations);
        
    % Ajout de la courbe
    SX = [ SX sx_j' ];
    SY = [ SY sy_j' ];
    SZ = [ SZ sz_j' ];
    
end


end