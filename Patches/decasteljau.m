function [ x, y, z ] = decasteljau( X, Y, Z, n, t )

    while n > 1
        for i = 1:n-1
           X(i) = (1-t) * X(i) + t * X(i+1);
           Y(i) = (1-t) * Y(i) + t * Y(i+1);
           Z(i) = (1-t) * Z(i) + t * Z(i+1);
        end
        
        n = n - 1;
    end
    
    x = X(1);
    y = Y(1);
    z = Z(1);
    
end