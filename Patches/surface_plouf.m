function [ X, Y, Z, n, m ] = surface_plouf()

[X,Y] = meshgrid(-10:1:10);
R = sqrt(X.^2 + Y.^2) + eps;
Z = sin(R)./R;

X = X / 20 + 0.5;
Y = Y / 20 + 0.5;

n = size(X, 1);
m = size(X, 2);

end

