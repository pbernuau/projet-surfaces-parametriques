clear;
close all;

% Bords du carreau
Fs0 = @(x) [ x 0 -x*(1-x)*5 ]';
Fs1 = @(x) [ x 1 -x*(1-x)*5 ]';
F0t = @(y) [ 0 y -y*(1-y)*5+sin(pi*y)*3 ]';
F1t = @(y) [ 1 y -y*(1-y)*5-sin(pi*y)*3 ]';


% Fs0 = @(x) transpose(decasteljau( [ 0 0 ], [ 1 0 ], [ 0 0 ], 2, x ));
% Fs1 = @(x) transpose(decasteljau( [ 0 1 ], [ 1 1 ], [ 0 0 ], 2, x ));
% F0t = @(x) transpose(decasteljau( [ 0 0 ], [ 0 1 ], [ 0 0 ], 2, x ));
% F1t = @(x) transpose(decasteljau( [ 1 0 ], [ 1 1 ], [ 0 0 ], 2, x ));



% Paramètres de Coon
n = 20;
m = 20;

% Calcul
[ CX, CY, CZ ] = coon( Fs0, Fs1, F0t, F1t, n, m );

% Préparation de l'affichage
figure('Name', 'Coon', 'Position', [0,0,550,500]);
axis([ 0 1 0 1 -5 5 ]);
grid on;
rotate3d on;
hold on;

% Affichage des bords
plotfunc(Fs0, (0:n)/n, 'r*');
plotfunc(Fs1, (0:n)/n, 'r*');
plotfunc(F0t, (0:m)/m, 'g*');
plotfunc(F1t, (0:m)/m, 'g*');


% Affichage du carreau de Coon
plotgrid(CX, CY, CZ, 'b');
plotgrid(CX', CY', CZ', 'b');


% Fin
hold off;
