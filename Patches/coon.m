function [ CX, CY, CZ ] = coon( Fs0, Fs1, F0t, F1t, n, m )
%COON Retourne le carreau de Coon associé aux fonctions données

    CX = zeros(n+1, m+1);
    CY = zeros(n+1, m+1);
    CZ = zeros(n+1, m+1);

    f1 = @(s,t) (1-t) * Fs0(s) + t * Fs1(s);
    f2 = @(s,t) (1-s) * F0t(t) + s * F1t(t);
    
    F00 = Fs0(0);
    F01 = F0t(1);
    F10 = F1t(0);
    F11 = Fs1(1);

    f = @(s,t)  f1(s,t) +                                               ...
                f2(s,t) -                                               ...
                ((1-s)*(1-t)*F00 + (1-s)*t*F01 + s*(1-t)*F10 + s*t*F11);
        
    for i = 0:n
        for j = 0:m
            Fst = f(i / n, j / m);
            
            CX(i+1, j+1) = Fst(1);
            CY(i+1, j+1) = Fst(2);
            CZ(i+1, j+1) = Fst(3);
        end
    end

end

