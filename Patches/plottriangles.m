function plottriangles(X, Y, Z, IJK, color)

triangles = [];

% recherche des triangles

for i = 1:length(IJK)
    k1 = 0;
    k = 0;
    point = IJK(i,:);
    [M,I] = max(point);
    if I+1 == 4 
        k = 1;
    else 
        k = I+1;
    end
    if I+2 == 4
        k1 = 1;
    elseif I+2 == 5
        k1 = 2;
    else
        k1 = I+2;
    end
    next_pt_1 = intersect(find(IJK(:,k) == point(k)+1), find(IJK(:,I) == M-1));
    next_pt_2 = intersect(find(IJK(:,k1) == point(k1)+1), find(IJK(:,I) == M-1));
    triangles = [triangles; i next_pt_1 next_pt_2];
    
end

% affichage des triangles

for i = 1:length(triangles)
    
    plot3([X(triangles(i,1)) X(triangles(i,2)) X(triangles(i,3)) X(triangles(i,1))], ...
          [Y(triangles(i,1)) Y(triangles(i,2)) Y(triangles(i,3)) Y(triangles(i,1))], ...
          [Z(triangles(i,1)) Z(triangles(i,2)) Z(triangles(i,3)) Z(triangles(i,1))],color);
           
end
    