function [ NX, NY, NZ ] = bruiter( X, Y, Z, n, m, coef_bruit )
%BRUITER Ajoute un bruit aléatoire à chaque point du maillage
% X, Y, Z
% n, m
% coef_bruit

NX = X + coef_bruit * 2 * (rand(n, m) - 0.5);
NY = Y + coef_bruit * 2 * (rand(n, m) - 0.5);
NZ = Z + coef_bruit * 2 * (rand(n, m) - 0.5);

end

