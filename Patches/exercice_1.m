clear;
close all;

% Paramètres de la grille
[ X, Y, Z, n, m ] = surface_plouf();

[ X, Y, Z ] = bruiter(X, Y, Z, n, m, 0.05);

% Nombre de pas
usteps = 50;
vsteps = 50;

% Paramètres de la spline
degre = 2;
iterations = 2;

[ BX, BY, BZ ] = bezier(X, Y, Z, n, m, usteps, vsteps);
[ SX, SY, SZ ] = bspline(X, Y, Z, n, m, degre, iterations);

% Affichage des points de contrôle

figure('Name', 'Points de contrôle', 'Position', [0,0,550,500]);
axis([ 0 1 0 1 min(min(Z)) max(max(Z)) ]);
grid on;
rotate3d on;
hold on;

plotgrid(X, Y, Z, 'b');

hold off;

% Affichage de la surface de Bézier

figure('Name', 'Surface de Bézier', 'Position', [550,0,550,500]);
axis([ 0 1 0 1 min(min(Z)) max(max(Z)) ]);
grid on;
rotate3d on;
hold on;

plotgrid(BX, BY, BZ, 'r');

hold off;

% Affichage de la spline

figure('Name', 'Spline', 'Position', [1100,0,550,500]);
axis([ 0 1 0 1 min(min(Z)) max(max(Z)) ]);
grid on;
rotate3d on;
hold on;

plotgrid(SX, SY, SZ, 'g');

hold off;
