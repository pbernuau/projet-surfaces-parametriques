function patch_triangulaire(X, Y, Z, IJK, n, usteps)
   
    PC = [X Y Z];
    
    for u = 0:usteps-1
        for v = 0:usteps-1-u
            P1 = bezier_patch(PC, IJK, n, u/usteps, v/usteps);
            P2 = bezier_patch(PC, IJK, n, (u+1)/usteps, v/usteps);
            P3 = bezier_patch(PC, IJK, n, u/usteps, (v+1)/usteps);
            P = [P1; P2; P3; P1];
            plot3(P(:,1), P(:,2), P(:,3), 'r');
        end
    end
end