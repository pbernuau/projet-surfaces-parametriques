function [ y ] = bernstein( k, n, t )

    y = nchoosek(n, k) * (1-t)^(n-k) * t^k;

end

