function [P] = bezier_patch(PC,IJK,n,u,v)
% PC        coordonnées des points de contrôle
%           de taille (n+1)(n+2)/2 x 3
% IJK       indices de chaque point : permet de retrouver
%           la topologie du triangle, de taille (n+1)(n+2)/2 x 3
% n         degré du patch triangulaire
% u,v       point d'évaluation : u,v => 0 et u+v<=1

    P = zeros(1,3);
    for i = 1:(n+1)*(n+2)/2
        P = P + PC(i,:)*bernstein_patch(n,u,v,IJK(i,1),IJK(i,2),IJK(i,3));
    end
    
end