function z = bernstein_patch(n,u,v,i,j,k)
% u,v >= 0 et u+v <= 1
% i+j+k = n

    z = factorial(n) * u^i * v^j * (1-u-v)^k / ...
        (factorial(i)*factorial(j)*factorial(k));
    
end
