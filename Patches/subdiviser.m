function [ sx_i, sy_i, sz_i ] = subdiviser( X, Y, Z, m, degre, iterations )
% Calcule les subdivisions successives de la courbe X, Y, Z
% X, Y, Z       points de la courbe à subdiviser (matrices 1*m)
% m             nombre de points
% degre         degré de la spline
% iterations    nombre de subdivisions à faire

if iterations > 0
    % dupliquer les points
    X = reshape([ X ; X ], 1, 2*m);
    Y = reshape([ Y ; Y ], 1, 2*m);
    Z = reshape([ Z ; Z ], 1, 2*m);
    
    X0 = X(:,1);        Xm = X(:,2*m);
    Y0 = Y(:,1);        Ym = Y(:,2*m);
    Z0 = Z(:,1);        Zm = Z(:,2*m);
    
    % répéter : prendre la moyenne de deux points consécutifs
    for j = 1:degre
        X(:,1:2*m-j) = (X(:,1:2*m-j) + X(:,2:2*m-j+1)) / 2;
        Y(:,1:2*m-j) = (Y(:,1:2*m-j) + Y(:,2:2*m-j+1)) / 2;
        Z(:,1:2*m-j) = (Z(:,1:2*m-j) + Z(:,2:2*m-j+1)) / 2;
    end
    
    X = [ X0 X(:,1:2*m-degre) Xm ];
    Y = [ Y0 Y(:,1:2*m-degre) Ym ];
    Z = [ Z0 Z(:,1:2*m-degre) Zm ];
        
    [ sx_i, sy_i, sz_i ] = subdiviser(X, Y, Z, 2*m - degre + 2, degre, iterations - 1);
else
    sx_i = X;
    sy_i = Y;
    sz_i = Z;
end

end
