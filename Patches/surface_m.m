function [ X, Y, Z, n, m ] = surface_m()

% Taille de la grille des points de contrôle
n = 5;
m = 6;

% Points de contrôle
X = repmat([0 0.2 0.4 0.6 0.8 1.0], 5, 1);
Y = repmat([0 ; 0.25 ; 0.5 ; 0.75 ; 1 ], 1, 6);
Z = repmat([0 2 0.75 0.75 2 0 ], 5, 1);

end

