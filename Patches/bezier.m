function [ BX, BY, BZ ] = bezier( X, Y, Z, n, m, usteps, vsteps )
%BEZIER Calcule une surface de Bézier
% X, Y, Z       coordonnées des points de contrôle (matrices n*m)
% n, m          dimensions de la matrice des points de contrôle
% usteps        nombre de valeurs à interpoler selon l'axe X
% vsteps        nombre de valeurs à interpoler selon l'axe Y

% L'algorithme fonctionne en deux étapes, d'abord sur les lignes, puis
% sur les colonnes

BX_tmp = [];
BY_tmp = [];
BZ_tmp = [];

BX = [];
BY = [];
BZ = [];

for i = 1:n
    
    bx_i = [];
    by_i = [];
    bz_i = [];
    
    % Calcul d'une courbe
    for u = 1:usteps+1
        [ x y z ] = decasteljau(X(i,:), Y(i,:), Z(i,:), m, (u-1) / usteps);
        
        bx_i = [ bx_i x ];
        by_i = [ by_i y ];
        bz_i = [ bz_i z ];
        
    end
    
    % Ajout de la courbe
    BX_tmp = [ BX_tmp ; bx_i ];
    BY_tmp = [ BY_tmp ; by_i ];
    BZ_tmp = [ BZ_tmp ; bz_i ];
    
end

for u = 1:usteps+1
    
    bx_u = [];
    by_u = [];
    bz_u = [];
    
    % Calcul d'une courbe
    for v = 1:vsteps+1
        [ x y z ] = decasteljau(BX_tmp(:,u)', BY_tmp(:,u)', BZ_tmp(:,u)', n, (v-1) / vsteps);
        %disp(x, y, z);
        
        bx_u = [ bx_u ; x ];
        by_u = [ by_u ; y ];
        bz_u = [ bz_u ; z ];
        
    end
    
    % Ajout de la courbe
    BX = [ BX bx_u ];
    BY = [ BY by_u ];
    BZ = [ BZ bz_u ];
    
end


end