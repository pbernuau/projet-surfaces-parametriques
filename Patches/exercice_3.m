clear;
close all;

tic 
% Paramètres de la grille
% PC = [0 0 0; 1 0 0; 0 1 0; 0.33 0 0.5; ...
%      0.66 0 0.5; 0 0.33 0.5; 0 0.66 0.5; ...
%      0.5 0.5 1; 0.33 0.75 0.5; 0.75 0.33 0.5 ];
% IJK = [3 0 0; 0 0 3; 0 3 0; 2 0 1; ...
%       1 0 2; 2 1 0; 1 2 0; 1 1 1; ...
%       0 2 1; 0 1 2];
PC = [0 0 0; 1 0 0; 0 1 0; 0.5 0 0.5; 0 0.5 0.5; 0.5 0.5 1];
IJK = [2 0 0; 0 0 2; 0 2 0; 1 0 1; 1 1 0; 0 1 1];
X = PC(:,1); Y = PC(:,2); Z = PC(:,3);

% Paramètres du patch
n = 2;

% Nombre de pas 
usteps = 70;

% Affichage des triangles de contrôle 

figure('Name', 'Triangles de contrôle', 'Position', [0,0,550,500]);
axis([ 0 1 0 1 min(min(Z)) max(max(Z)) ]);
grid on;
rotate3d on;
hold on;
plottriangles(X, Y, Z, IJK, 'b');

% Calcul et affichage du patch triangulaire

patch_triangulaire(X, Y ,Z, IJK, n, usteps);
toc 
hold off;



      