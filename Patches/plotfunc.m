function plotfunc( F, tval, color )

    n = size(tval, 2);
    
    X = zeros(n, 1);
    Y = zeros(n, 1);
    Z = zeros(n, 1);
    
    for i = 1:n
       P = F(tval(i));
       
       X(i) = P(1);
       Y(i) = P(2);
       Z(i) = P(3);
    end
    
    plot3(X', Y', Z', color);

end

