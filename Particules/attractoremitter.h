#ifndef ATTRACTOREMITTER_H
#define ATTRACTOREMITTER_H

#include "color.h"
#include "config.h"
#include "emitter.h"
#include "particle.h"
#include "random.h"
#include "textures.h"

#include <iostream>
#include <list>

#include <math.h>

class AttractorEmitter : public Emitter
{
	
public:
	AttractorEmitter(Engine* e) :
		Emitter(e),
		elapsedTime(0.f)
	{
	}
	
	~AttractorEmitter()
	{
		for (
				std::list<Particle*>::iterator it = particles.begin();
				it != particles.end();
				it++
			)
		{
			delete *it;
		}
		
		particles.clear();
	}
	
	virtual void Update(float dt)
	{
		static const unsigned int kTexture = LoadTexture(TEXTURE_FLARE);
		static const float kSpawnInterval = 1.f / SMOKE_PARTICLES_PER_SECOND;
				
		elapsedTime = elapsedTime + dt;
		while (elapsedTime > kSpawnInterval)
		{
			Particle* p = new Particle();
			p->pos = vec3(0.f, 50.f, 25.f);
			p->vel = vec3(0.f, 15.f, 5.f);
			p->color.g = 0.f;
			p->color.b = 0.f;
			
			p->texture = kTexture;
		
			engine->AddParticle(p);
			particles.push_back(p);
		
			elapsedTime = elapsedTime - kSpawnInterval;
		}
		
		UpdateParticles();
	}
	
protected:
	void UpdateParticles()
	{
		const vec3 G = vec3(50.f, 50.f, 25.f);
		
		for (
				std::list<Particle*>::iterator it = particles.begin();
				it != particles.end();
			)
		{
			Particle* p = *it;
			
			if (!p->alive)
			{
				delete p;
				particles.erase(it++);
			}
			else
			{
				vec3 dp = G - p->pos;
				float dist = dp.Length();
				
				if (dist < 1e-2f)
				{
					p->acc = vec3();
					if (p->vel.Length() < 1e-2f)
						p->alive = false;
				}
				else
				{
					p->acc = 500.f * dp / (dist * dist);
				}
				
				it++;
			}
		}
	}
	
private:
	std::list<Particle*>		particles;
	float						elapsedTime;
};

#endif // ATTRACTOREMITTER_H

