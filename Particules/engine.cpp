#include "engine.h"
#include "textures.h"
#include "utils.h"

#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/freeglut.h>
#include <GL/glu.h>

#include <algorithm>
#include <iostream>
#include <vector>

struct ParticleInfo
{
	Particle*	particle;
	float		distance;
	
	bool operator<(const ParticleInfo& other) const
	{
		return distance < other.distance;
	}
};

Engine::Engine() :
	paused(true),
	gridVisible(true),
	time(0.f),
	camera(vec3(50.f, -100.f, 50.f), -5.f, 90.f),
	move(0),
	strate(0)
{
}

void Engine::Update(float dt)
{
	if (move != 0)
		camera.Move(dt * move);

	if (strate != 0)
		camera.Strate(dt * strate);
	
	if (!paused)
	{
		time += dt;
		UpdateParticles(dt);
	}
}

void Engine::Render()
{		
	camera.Look();
	
	if (gridVisible)
		DrawGrid(10);
	
	DrawAxes();
	
	RenderParticles();
}

void Engine::KeyEvent(unsigned char key, bool up)
{
	switch (key)
	{
	case 'z':
		move = up ? 0 : 1;
		break;

	case 's':
		move = up ? 0 : -1;
		break;

	case 'q':
		strate = up ? 0 : 1;
		break;

	case 'd':
		strate = up ? 0 : -1;
		break;
		
	case ' ':
		paused = up ? paused : !paused;
		break;
		
	case 'g':
		gridVisible = up ? gridVisible : !gridVisible;
		break;
		
	case '\033': // ESC
		exit(0);
		break;
	}
}

void Engine::SpecialKeyEvent(int key, bool up)
{
}

void Engine::MouseButtonEvent(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		leftButton = state;
		break;
	
	case GLUT_RIGHT_BUTTON:
		rightButton = state;
		break;
	}
	
	if (leftButton == GLUT_DOWN)
	{
		lastMouseX = x;
		lastMouseY = y;
	}
}

void Engine::MouseMoveEvent(int x, int y, bool passive)
{
	if (passive || leftButton == GLUT_UP)
		return;
		
	int dx = x - lastMouseX;
	int dy = y - lastMouseY;
	
	camera.Rotate(dx, dy);
	
	lastMouseX = x;
	lastMouseY = y; 
}

void Engine::UpdateParticles(float dt)
{
	for (	std::list<Particle*>::iterator it = particles.begin();
			it != particles.end(); )
	{
		Particle& p = **it;
		
		if (p.alive)
		{
			p.vel += p.acc * dt;
			p.pos += p.vel * dt;
			
			it++;
		}
		else
		{
			particles.erase(it++);
		}
    }
}

void Engine::RenderParticles()
{	
	// Trier les particules de la plus éloignée à la plus proche
	std::vector<ParticleInfo> visible;
	visible.reserve(particles.size());
	
	for (	std::list<Particle*>::iterator it = particles.begin();
			it != particles.end();
			it++ )
	{
		float dist = camera.SquaredDistanceFromScreen((*it)->pos);
		if (dist > 0.f)
		{
			ParticleInfo info = { *it, dist }; 
			visible.push_back(info);
		}
	}
	std::sort(visible.begin(), visible.end());
	
	// Calcul de la matrice de vue des sprites
	GLfloat mat[16];
	glGetFloatv(GL_MODELVIEW_MATRIX , mat);
	
	mat[12] = 0.f;
	mat[13] = 0.f;
	mat[14] = 0.f;
	mat[15] = 1.f;
	
	std::swap(mat[1], mat[4]);
	std::swap(mat[2], mat[8]);
	std::swap(mat[6], mat[9]);
	std::swap(mat[3], mat[12]);
	std::swap(mat[7], mat[13]);
	std::swap(mat[11], mat[14]);
	
	// Pour l'affichage des particules, on désactive l'écriture du depth-buffer
	// et on active les textures
	glDepthMask(GL_FALSE);
	glEnable(GL_TEXTURE_2D);
	
	for (	std::vector<ParticleInfo>::reverse_iterator it = visible.rbegin();
			it != visible.rend();
			it++ )
	{
		const Particle* p = it->particle;
		
		glBindTexture(GL_TEXTURE_2D, p->texture);
		glColor4fv(p->color.array);
		
		glPushMatrix();
	
		glTranslatef(p->pos.x, p->pos.y, p->pos.z);

		glMultMatrixf(mat);
		glScalef(p->size, p->size, p->size);
		glRotatef(p->angle, 0.f, 0.f, 1.f);
	
		glBegin(GL_QUADS);
		glTexCoord2i(0,0); glVertex3i(-1, -1, 0);
		glTexCoord2i(1,0); glVertex3i(+1, -1, 0);
		glTexCoord2i(1,1); glVertex3i(+1, +1, 0); 
		glTexCoord2i(0,1); glVertex3i(-1, +1, 0);
		glEnd();
	
		glPopMatrix();
	}
	
	glDisable(GL_TEXTURE_2D);
	glDepthMask(GL_TRUE);
}



