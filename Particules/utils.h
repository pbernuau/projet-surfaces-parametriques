#ifndef UTILS_H
#define UTILS_H

#include "vec3.h"

void DrawAxes(float length = 10.f, const vec3 origin = vec3());

void DrawGrid(
	int n,
	const vec3 origin = vec3(),
	const vec3 dp = vec3(10.f, 10.f, 0.f));

#endif // UTILS_H



