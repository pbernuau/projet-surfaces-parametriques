#include "textures.h"

#include "images.h"

#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/gl.h>

template<unsigned int width, unsigned int height, unsigned int components>
GLuint GenTexture(const unsigned char (&data)[width][height][components])
{
	GLuint texture;
	
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glTexImage2D(GL_TEXTURE_2D, 0, components, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	return texture;
}

unsigned int LoadTexture(TextureName name)
{
	switch (name)
	{
	case TEXTURE_SMOKE1:
		return GenTexture(smoke);
	
	case TEXTURE_SMOKE2:
		return GenTexture(smoke2);
		
	case TEXTURE_SMOKE3:
		return GenTexture(smoke3);
		
	case TEXTURE_FLARE:
		return GenTexture(flare);
		
	case TEXTURE_SPARK:
		return GenTexture(spark);
	
	default:
		return -1;
	}
}

