#ifndef FREEFLYCAMERA_H
#define FREEFLYCAMERA_H

#include "vec3.h"

//
// FreeFlyCamera
//
// This class enables the user to move freely in the scene
// using the mouse and the keyboard.
//
class FreeFlyCamera
{
public:
	FreeFlyCamera(const vec3& position, float phi, float theta);
	~FreeFlyCamera();

	// dt > 0 => move forward
	// dt < 0 => move backward
	void Move(float dt);
	// dt > 0 => strate left
	// dt < 0 => strate right
	void Strate(float dt);

	void Rotate(float dx, float dy);

	void SetSpeed(float speed);
	void SetSensivity(float sensivity);

	void SetPosition(const vec3& position);
	void SetTarget(const vec3& target);

	const vec3& Position() const;
	const vec3& Target() const;

	void Look();
  
	float SquaredDistanceFromScreen(const vec3& point) const;

protected:
	void VectorsFromAngles();

private:
	float	_speed,
			_sensivity;

	vec3	_position,
			_target,
			_forward,
			_left;

	float	_theta,
			_phi;
};

#endif //FREEFLYCAMERA_H

