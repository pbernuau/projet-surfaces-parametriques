#ifndef SMOKEEMITTER_H
#define SMOKEEMITTER_H

#include "color.h"
#include "config.h"
#include "emitter.h"
#include "particle.h"
#include "random.h"
#include "textures.h"

#include <iostream>
#include <list>

#include <math.h>

class SmokeEmitter : public Emitter
{
	struct SmokeParticle : public Particle
	{
		float	spawn;
	};
	
public:
	SmokeEmitter(Engine* e) :
		Emitter(e),
		elapsedTime(0.f)
	{
	}
	
	~SmokeEmitter()
	{
		for (
				std::list<SmokeParticle*>::iterator it = particles.begin();
				it != particles.end();
				it++
			)
		{
			delete *it;
		}
		
		particles.clear();
	}
	
	virtual void Update(float dt)
	{
		static const unsigned int kTexture = LoadTexture(SMOKE_TEXTURE);
		static const float kSpawnInterval = 1.f / SMOKE_PARTICLES_PER_SECOND;
		static const float kAngularFreq = 2.f * PI / SMOKE_EMITTER_PERIOD;
		
		UpdateParticles();
		
		float time = engine->Time();
		
		elapsedTime = elapsedTime + dt;
		while (elapsedTime > kSpawnInterval)
		{
			SmokeParticle* p = new SmokeParticle();
			p->pos = vec3(
				50.f + SMOKE_EMITTER_AMPLITUDE_X * cos(kAngularFreq * time),
				50.f + SMOKE_EMITTER_AMPLITUDE_Y * sin(kAngularFreq * time),
				10.f);
			p->size = 0.f;
			p->texture = kTexture;
			p->acc = vec3(
				SMOKE_INITIAL_ACC_MAGNITUDE_X * (Random::NextFloat()*2.f - 1.f),
				SMOKE_INITIAL_ACC_MAGNITUDE_Y * (Random::NextFloat()*2.f - 1.f),
				SMOKE_INITIAL_ACC_MAGNITUDE_Z * Random::NextFloat());
			p->spawn = time;
		
			engine->AddParticle(p);
			particles.push_back(p);
		
			elapsedTime = elapsedTime - kSpawnInterval;
		}
	}
	
protected:
	void UpdateParticles()
	{
		float time = engine->Time();
		
		for (
				std::list<SmokeParticle*>::iterator it = particles.begin();
				it != particles.end();
			)
		{
			SmokeParticle* p = *it;
			
			if (!p->alive)
			{
				delete p;
				particles.erase(it++);
			}
			else
			{
				float age = time - p->spawn;
				//p->acc.x += 0.01f * sin(time/10.f);
				p->size = SMOKE_SIZE_MAX * (1.f - exp(-age / SMOKE_SIZE_TAU));
				p->color.a = exp(-age / SMOKE_COLOR_ALPHA_TAU);
				
				if (age > 5.f * SMOKE_COLOR_ALPHA_TAU)
					p->alive = false;
				
				it++;
			}
		}
	}
	
private:
	std::list<SmokeParticle*>	particles;
	float						elapsedTime;
};

#endif // SMOKEEMITTER_H

