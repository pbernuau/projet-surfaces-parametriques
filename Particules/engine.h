#ifndef ENGINE_H
#define ENGINE_H

#include "freeflycamera.h"
#include "particle.h"

#include <list>

class Engine
{
public:
	Engine();
	
	bool IsPaused() const;
	void Pause();
	void Resume();
	
	bool IsGridVisible() const;
	void SetGridVisible(bool visible);

	float Time() const;
	
	void AddParticle(Particle* p);
	int ParticleCount() const;
	
	void Reset();
	void Update(float dt);

	void Render();
	
	void KeyEvent(unsigned char key, bool up);
	void SpecialKeyEvent(int key, bool up);
	void MouseButtonEvent(int button, int state, int x, int y);
	void MouseMoveEvent(int x, int y, bool passive);
	
protected:
	void UpdateParticles(float dt);
	void RenderParticles();

private:
	bool					paused;
	bool					gridVisible;
	
	float					time;
	std::list<Particle*>	particles;
	
	int						leftButton;
	int						rightButton;
	
	FreeFlyCamera			camera;
	int						lastMouseX, lastMouseY;
	int 					move;
	int						strate;
};

inline bool Engine::IsPaused() const
{
	return paused;
}

inline void Engine::Pause()
{
	paused = true;
}

inline void Engine::Resume()
{
	paused = false;
}

inline bool Engine::IsGridVisible() const
{
	return gridVisible;
}

inline void Engine::SetGridVisible(bool visible)
{
	gridVisible = visible;
}

inline float Engine::Time() const
{
	return time;
}

inline void Engine::AddParticle(Particle* p)
{
	particles.push_back(p);
}

inline int Engine::ParticleCount() const
{
	return particles.size();
}

inline void Engine::Reset()
{
	time = 0.f;
	particles.clear();
}

#endif // ENGINE_H

