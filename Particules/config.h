#ifndef CONFIG_H
#define CONFIG_H

#define PI 3.14159265359f

//
// Paramètres de l'émetteur d'explosions
//

// Durée maximale entre deux explosions, en seconde (float)
#define EXPL_INTERVAL_MAX				2.f
// Taille des particules émises (float)
#define EXPL_PARTICLE_SIZE				1.f
// Nombre minimum de particules par explosion (int)
#define EXPL_PARTICLE_COUNT_BASE		200
// Nombre maximum de particules ajoutées aléatoirement (int)
#define EXPL_PARTICLE_COUNT_MAX			200
// Si vrai, les vitesses initiales des particules seront de même norme
#define EXPL_SPHERICAL					1
// Masse d'une particule, en kg (float)
#define EXPL_PARTICLE_MASS				0.010f
// Coefficient de frottement fluide de l'air (float)
#define EXPL_AIR_FRICTION				0.01f
// Magnitude de la vitesse initiale d'une particule (float)
#define EXPL_INTIAL_VEL_MAGNITUDE		50.f
// Constante de temps régissant l'opacité d'une particule, en seconde (float)
#define EXPL_COLOR_ALPHA_TAU			2.5f


//
// Paramètres de l'émetteur de fumée
//

// Texture des particules (TEXTURE_SMOKE2, TEXTURE_SMOKE3 ou TEXTURE_FLARE)
#define SMOKE_TEXTURE					TEXTURE_FLARE
// Nombre de particules par seconde (int)
#define SMOKE_PARTICLES_PER_SECOND		50
// Amplitude du mouvement de l'émetteur selon l'axe x (float)
#define SMOKE_EMITTER_AMPLITUDE_X		0.f
// Amplitude du mouvement de l'émetteur selon l'axe y (float)
#define SMOKE_EMITTER_AMPLITUDE_Y		0.f
// Période, en hertz, du mouvement de l'émetteur (float)
#define SMOKE_EMITTER_PERIOD			10.f
// Magnitude de l'accélération initiale d'une particule selon l'axe x (float)
#define SMOKE_INITIAL_ACC_MAGNITUDE_X	1.f
// Magnitude de l'accélération initiale d'une particule selon l'axe y (float)
#define SMOKE_INITIAL_ACC_MAGNITUDE_Y	1.f
// Magnitude de l'accélération initiale d'une particule selon l'axe z (float)
#define SMOKE_INITIAL_ACC_MAGNITUDE_Z	4.f
// Taille asymptotique d'une particule
#define SMOKE_SIZE_MAX					10.f
// Constante de temps régissant la taille d'une particule, en seconde (float)
#define SMOKE_SIZE_TAU					2.f
// Constante de temps régissant l'opacité d'une particule, en seconde (float)
#define SMOKE_COLOR_ALPHA_TAU			2.f



#endif // CONFIG_H

