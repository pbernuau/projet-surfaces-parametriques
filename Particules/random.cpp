#include "random.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

void Random::Init()
{
	srand(time(NULL));
}

int Random::NextInt(int max)
{
	return rand() % max;
}

float Random::NextFloat()
{
	return rand() / float(RAND_MAX);
}

vec3 Random::NextVector()
{
	float z = 2.f * (NextFloat() - 0.5f);
	float t = 2.f * PI * NextFloat();
	
	float r = sqrt(1 - z*z);
	
	return vec3(r * cos(t), r * sin(t), z);
}



