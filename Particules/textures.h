#ifndef TEXTURES_H
#define TEXTURES_H

enum TextureName
{
	TEXTURE_SMOKE1,
	TEXTURE_SMOKE2,
	TEXTURE_SMOKE3,
	TEXTURE_FLARE,
	TEXTURE_SPARK
};

unsigned int LoadTexture(TextureName name);

#endif // TEXTURES_H

