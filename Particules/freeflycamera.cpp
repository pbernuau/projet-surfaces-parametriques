#include "freeflycamera.h"

#include "config.h"

#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/freeglut.h>
#include <GL/glu.h>

#include <math.h>

FreeFlyCamera::FreeFlyCamera(const vec3& position, float phi, float theta)
{
	_speed = 50.f;
	_sensivity = 0.2f;

	_position = position;
	_phi = phi;
	_theta = theta;

	VectorsFromAngles();
}

FreeFlyCamera::~FreeFlyCamera()
{
}

void FreeFlyCamera::Move(float dt)
{
	_position += _forward * _speed * dt;
	_target = _position + _forward;
}

void FreeFlyCamera::Strate(float dt)
{
	_position += _left * _speed * dt;
	_target = _position + _forward;
}

void FreeFlyCamera::Rotate(float dx, float dy)
{
	_theta -= dx * _sensivity;
	_phi -= dy * _sensivity;
	VectorsFromAngles();
}

void FreeFlyCamera::SetSpeed(float speed)
{
	_speed = speed;
}

void FreeFlyCamera::SetSensivity(float sensivity)
{
	_sensivity = sensivity;
}

void FreeFlyCamera::SetPosition(const vec3& position)
{
	_position = position;
	_target = _position + _forward;
}

void FreeFlyCamera::SetTarget(const vec3& target)
{
	_target = target;
}

const vec3& FreeFlyCamera::Position() const
{
	return _position;
}

const vec3& FreeFlyCamera::Target() const
{
	return _target;
}

void FreeFlyCamera::VectorsFromAngles()
{
	static const vec3 up(0.f, 0.f, 1.f);
	
	if (_phi > 89.f)
	{
		_phi = 89.f;
	}
	else if (_phi < -89.f)
	{
		_phi = -89.f;
	}

	float r_temp = cos(_phi * PI / 180.f);
	_forward.z = sin(_phi * PI / 180.f);
	_forward.x = r_temp*cos(_theta * PI / 180.f);
	_forward.y = r_temp*sin(_theta * PI / 180.f);

	_left = up.Cross(_forward);
	_left.Normalize();

	_target = _position + _forward;
}

void FreeFlyCamera::Look()
{
	gluLookAt(	_position.x, _position.y, _position.z,
		    	_target.x, _target.y, _target.z,
		    	0.f, 0.f, 1.f);
}

float FreeFlyCamera::SquaredDistanceFromScreen(const vec3& point) const
{
	return _forward.Dot(point - _position);
}

