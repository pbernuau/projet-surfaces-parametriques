#include "utils.h"

#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/gl.h>

void DrawAxes(float length, const vec3 origin)
{
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
	
	glTranslatef(origin.x, origin.y, origin.z);
	glScalef(length, length, length);

	glColor4f(1.f, 0.f, 0.f, 1.f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(1, 0, 0);
	glEnd();

	glColor4f(0.f, 1.f, 0.f, 1.f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(0, 1, 0);
	glEnd();

	glColor4f(0.f, 0.f, 1.f, 1.f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(0, 0, 1);
	glEnd();

	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void DrawGrid(int n, const vec3 origin, const vec3 dp)
{
	glPushMatrix();

	glTranslatef(origin.x, origin.y, origin.z);
	glScalef(dp.x, dp.y, dp.z);
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if ((i + j) % 2)
				glColor4f(0.0f, 0.0f, 1.f, 1.f);
			else
				glColor4f(1.f, 1.f, 1.f, 1.f);
			
			glBegin(GL_QUADS);
			glVertex3i(i, j, 0);
			glVertex3i(i, j+1, 0);		
			glVertex3i(i+1, j+1, 0);
			glVertex3i(i+1, j, 0);
			glEnd();
		}
		
	}

	glPopMatrix();
}


