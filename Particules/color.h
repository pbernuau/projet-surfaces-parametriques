#ifndef COLOR_H
#define COLOR_H

struct Color
{
	union
	{
		struct
		{
			float r, g, b, a;
		};
		
		float array[4];
	};
	
	Color() : r(1.f), g(1.f), b(1.f), a(1.f)
	{
	}
	
	operator float*()
	{
		return &r;
	}
};


#endif // COLOR_H

