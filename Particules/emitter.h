#ifndef EMITTER_H
#define EMITTER_H

class Engine;

class Emitter
{
public:
	Emitter(Engine* e) :
		engine(e)
	{
	}
	
	virtual ~Emitter() {}
	
	virtual void Update(float dt) = 0;
	
protected:
	Engine* engine;
	
};

#endif // EMITTER_H

