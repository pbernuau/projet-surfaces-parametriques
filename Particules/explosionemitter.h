#ifndef EXPLOSIONEMITTER_H
#define EXPLOSIONEMITTER_H

#include "color.h"
#include "config.h"
#include "emitter.h"
#include "random.h"
#include "textures.h"

#include <iostream>

#include <math.h>

class ExplosionEmitter : public Emitter
{
	struct Explosion
	{
		float		date;
		bool		finished;
		Particle*	particles;
		int			count;
	};
	
public:
	ExplosionEmitter(Engine* e) :
		Emitter(e),
		explosionTimeout(1.f)
	{
	}
	
	~ExplosionEmitter()
	{
		for (
				std::list<Explosion*>::iterator it = explosions.begin();
				it != explosions.end();
				it++
			)
		{			
			delete[] (*it)->particles;
			delete *it;
		}
		
		explosions.clear();
	}
	
	virtual void Update(float dt)
	{
		UpdateParticles();
	
		explosionTimeout -= dt;
		if (explosionTimeout < 0.f)
		{
			Explode();
			explosionTimeout += EXPL_INTERVAL_MAX * Random::NextFloat();
		}
	}
	
protected:
	void UpdateParticles()
	{
		static const vec3 g = vec3(0.f, 0.f, -9.81f);
		
		float time = engine->Time();
		
		for (
				std::list<Explosion*>::iterator it = explosions.begin();
				it != explosions.end();
			)
		{
			Explosion* expl = *it;
			
			if (expl->finished)
			{
				delete[] expl->particles;
				delete expl;
				
				explosions.erase(it++);
				continue;
			}
			
			float age = time - expl->date;
			
			if (age > 5.f * EXPL_COLOR_ALPHA_TAU)
			{
				for (int i = 0; i < expl->count; i++)
					expl->particles[i].alive = false;
					
				expl->finished = true;
			}
			
			for (int i = 0; i < expl->count; i++)
			{
				Particle* p = &expl->particles[i];
				p->acc = g - p->vel * EXPL_AIR_FRICTION / EXPL_PARTICLE_MASS;
				
				//if (age > 0.1f * kDuration)
				//	p.vel += 0.1f * Random::NextVector();
				
				p->color.a = exp(-age / EXPL_COLOR_ALPHA_TAU);
			}
					
			it++;
		}
	}
	
	void Explode()
	{
		static const unsigned int kTexture = LoadTexture(TEXTURE_FLARE);
	
		float time = engine->Time();
		
		vec3 origin = 50.f * Random::NextVector() + vec3(50.f, 50.f, 75.f);
		int count =
			EXPL_PARTICLE_COUNT_BASE +
			Random::NextInt(EXPL_PARTICLE_COUNT_MAX);
		
		float r = Random::NextFloat();
		float g = Random::NextFloat();
		float b = Random::NextFloat();
		
		Explosion* expl = new Explosion();
		expl->date = time;
		expl->finished = false;
		expl->particles = new Particle[count];
		expl->count = count;
		
		for (int i = 0; i < count; i++)
		{			
			Particle* p = &expl->particles[i];
			
			p->pos = origin;
			p->vel = EXPL_INTIAL_VEL_MAGNITUDE * Random::NextVector();
#if !(EXPL_SPHERICAL)
			p->vel *= Random::NextFloat();
#endif
			p->size = EXPL_PARTICLE_SIZE;			
			p->texture = kTexture;
			p->color.r = r;
			p->color.g = g;
			p->color.b = b;
			
			engine->AddParticle(p);
		}
		
		explosions.push_back(expl);
	}
	
private:
	std::list<Explosion*>	explosions;
	float					explosionTimeout;
};

#endif // EXPLOSIONEMITTER_H
