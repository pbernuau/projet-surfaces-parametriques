#include "engine.h"
#include "emitter.h"
#include "nullemitter.h"
#include "explosionemitter.h"
#include "smokeemitter.h"
#include "attractoremitter.h"
#include "random.h"

#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/freeglut.h>
#include <GL/glu.h>

#include <stdio.h>
#include <sstream>

#define LINE_HEIGHT		15
#define COLUMN_WIDTH	9
#define FONT			GLUT_BITMAP_9_BY_15

enum MenuEntry
{
	MENU_EMITTER_NULL,
	MENU_EMITTER_EXPLOSION,
	MENU_EMITTER_SMOKE,
	MENU_EMITTER_ATTRACTOR,
	MENU_TOGGLE_ANIMATION,
	MENU_TOGGLE_GRID
};

//
// Variables globales
//

static struct G
{
	G() :
		width(1024),
		height(768),
		engine(NULL),
		emitter(NULL)
	{
	}
	
	// Dimensions de la fenêtre
	int			width;
	int			height;
	
	// Moteur de particules et émetteur actuel
	Engine*		engine;
	Emitter*	emitter;
}* g = new G();

//
// Helpers
//

void RenderText(const char* str, int row, int col);

//
// GLUT callbacks
//

void Display(void);
void Reshape(int width, int height);

void KeyboardDown(unsigned char key, int x, int y);
void KeyboardUp(unsigned char key, int x, int y);
void Mouse(int button, int state, int x, int y);
void Motion(int x, int y);
void PassiveMotion(int x, int y);
void SpecialDown(int key, int x, int y);
void SpecialUp(int key, int x, int y);

void MenuSelect(int selection);

//
// Fonction main()
//

int main(int argc, char** argv)
{
	printf("Loading...\n");
	
	Random::Init();
	
    glutInit(&argc, argv);
    
    // Paramètres de la fenêtre
    glutInitWindowSize(g->width, g->height);
    glutInitWindowPosition(100, 100);
    
    // Définition des modes d'affichage
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    
    // Création de la fenêtre
    glutCreateWindow("Projet Interpolation");
    
    // Création du menu
	glutCreateMenu(MenuSelect);
	glutAddMenuEntry("Emitter: None",		MENU_EMITTER_NULL);
	glutAddMenuEntry("Emitter: Explosions",	MENU_EMITTER_EXPLOSION);
	glutAddMenuEntry("Emitter: Smoke",		MENU_EMITTER_SMOKE);
	glutAddMenuEntry("Emitter: Attractor",	MENU_EMITTER_ATTRACTOR);
	glutAddMenuEntry("Toggle Animation",	MENU_TOGGLE_ANIMATION);
	glutAddMenuEntry("Toggle Grid",			MENU_TOGGLE_GRID);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// Gestion des événements
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(KeyboardDown);
	glutKeyboardUpFunc(KeyboardUp);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutPassiveMotionFunc(PassiveMotion);
	glutSpecialFunc(SpecialDown);
	glutSpecialUpFunc(SpecialUp);

	// Configuration OpenGL
    glClearColor(0.f, 0.f, 0.f, 0.f);
    
    glEnable(GL_BLEND);
	
	glPointSize(2.f);
	glLineWidth(5.f);
    
    g->engine = new Engine();

	printf("Ready! (%i ms)\n", glutGet(GLUT_ELAPSED_TIME));

    glutMainLoop();
    
    return 0;
}

void RenderText(const char* str, int row, int col)
{
	glRasterPos2i(col * COLUMN_WIDTH, g->height - (row + 1) * LINE_HEIGHT);
	while (*str != '\0')
	{
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *(str++));
	}
}

void Display()
{
	//
	// Calcul des FPS
	//
	
	static unsigned int lastTime = glutGet(GLUT_ELAPSED_TIME);
	static unsigned int frameCounter = 0;
	static unsigned int timeCounter = 0;
	
	static struct
	{
		std::string fps;
		std::string particles;
		std::string time;
	} info;
	
	int time = glutGet(GLUT_ELAPSED_TIME);
	int ms = time - lastTime;
	
	timeCounter = timeCounter + ms;
	if (timeCounter > 1000)
	{
		std::ostringstream fps;
		fps << "FPS : " << frameCounter;
		info.fps = fps.str();
		
		std::ostringstream particles;
		particles << "Particles: " << g->engine->ParticleCount();
		info.particles = particles.str();
		
		std::ostringstream time;
		time << "Time: " << static_cast<int>(g->engine->Time());
		info.time = time.str();
		
		frameCounter = 0;
		timeCounter = timeCounter - 1000;
	}
	else
	{
		frameCounter++;
	}
	
	//
	// Mise à jour du moteur et de l'émetteur
	//
	
	float dt = float(ms) / 1000.f;

	if (g->emitter != NULL && !g->engine->IsPaused())
		g->emitter->Update(dt);
	
	g->engine->Update(dt);
	
	lastTime = time;
	
	//
	// Affichage
	//
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Scène 3D

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(
		45,
		float(g->width) / float(g->height),
		1,
		1000
	);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glEnable(GL_DEPTH_TEST);
		
	g->engine->Render();
	
	glDisable(GL_DEPTH_TEST);
	
	// Couche 2D
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, g->width, 0.0, g->height);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
		
	glColor4f(1.f, 0.f, 0.f, 1.f);
	RenderText(info.fps.c_str(), 0, 0);
	RenderText(info.particles.c_str(), 1, 0);
	RenderText(info.time.c_str(), 2, 0);
	
	// Fin
	
	glutSwapBuffers();
	glutPostRedisplay();
}

void Reshape(int width, int height)
{
	glViewport(0, 0, width, height);
	g->width = width;
	g->height = height;
}

void KeyboardDown(unsigned char key, int x, int y)
{
	(void)(x);
	(void)(y);
	g->engine->KeyEvent(key, false);
}

void KeyboardUp(unsigned char key, int x, int y)
{
	(void)(x);
	(void)(y);
	g->engine->KeyEvent(key, true);
}


void Mouse(int button, int state, int x, int y)
{
	g->engine->MouseButtonEvent(button, state, x, y);
}

void Motion(int x, int y)
{
	g->engine->MouseMoveEvent(x, y, false);
}

void PassiveMotion(int x, int y)
{
	g->engine->MouseMoveEvent(x, y, true);
}

void SpecialDown(int key, int x, int y)
{
	g->engine->SpecialKeyEvent(key, false);
}

void SpecialUp(int key, int x, int y)
{
	g->engine->SpecialKeyEvent(key, true);
}

void MenuSelect(int selection)
{
	Emitter* emitter = NULL;
	
	switch (selection)
	{
	case MENU_EMITTER_NULL:
		emitter = new NullEmitter(g->engine);
		break;
		
	case MENU_EMITTER_EXPLOSION:
		emitter = new ExplosionEmitter(g->engine);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		break;
	
	case MENU_EMITTER_SMOKE:
		emitter = new SmokeEmitter(g->engine);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
		
	case MENU_EMITTER_ATTRACTOR:
		emitter = new AttractorEmitter(g->engine);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		break;
		
	case MENU_TOGGLE_ANIMATION:
		if (g->engine->IsPaused())
			g->engine->Resume();
		else
			g->engine->Pause();
		break;
		
	case MENU_TOGGLE_GRID:
		g->engine->SetGridVisible(!g->engine->IsGridVisible());
		break;
	
	default:
		exit(-1);
	}
	
	if (emitter)
	{
		g->engine->Reset();
		
		if (g->emitter)
			delete g->emitter;
			
		g->emitter = emitter;
		g->engine->Resume();
	 }
}

