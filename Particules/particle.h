#ifndef PARTICLE_H
#define PARTICLE_H

#include "color.h"
#include "vec3.h"

#include "stdlib.h"

struct Particle
{
	Particle() :
		alive(true),
		size(1.f),
		angle(0.f),
		texture(-1)
	{
	}
	
	// Position, vitesse, accélération
	vec3 pos, vel, acc;
	
	bool alive;

	// Apparence
	float size, angle;
	unsigned int texture;
	Color color;
};

#endif // PARTICULE_H

