#include "vec3.h"

#include <math.h>

float vec3::Dot(const vec3& v) const
{
	return x*v.x + y*v.y + z*v.z;
}

vec3 vec3::Cross(const vec3& v) const
{
	return vec3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
}

float vec3::Length() const
{
	return sqrt(Dot(*this));
}

vec3& vec3::Normalize()
{
  return (*this) /= Length();
}

vec3 vec3::Normalized() const
{
	return (*this) / Length();
}


