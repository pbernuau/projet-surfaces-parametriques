#ifndef RANDOM_H
#define RANDOM_H

#include "config.h"
#include "vec3.h"

namespace Random
{
	void Init();
	
	int NextInt(int max);
	
	float NextFloat();
	
	vec3 NextVector();
}

#endif // RANDOM_H


