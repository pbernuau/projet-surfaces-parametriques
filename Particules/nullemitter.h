#ifndef NULLEMITTER_H
#define NULLEMITTER_H

#include "emitter.h"

class Engine;

class NullEmitter : public Emitter
{
public:
	NullEmitter(Engine* e) :
		Emitter(e)
	{
	}
		
	virtual void Update(float dt)
	{
	}	
};

#endif // NULLEMITTER_H

