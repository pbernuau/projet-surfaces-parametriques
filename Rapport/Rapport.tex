\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[top=3cm, bottom=3cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{graphicx}
\geometry{scale=1.0, nohead}

\title{Projet : Surfaces paramétriques}
\author{William Lefrançois - Paul Bernuau - Groupe F}

\begin{document}
\maketitle

\tableofcontents
\pagebreak

\section{Patches triangulaires}

\subsection{Modèle}

Les patches triangulaires utilisent le même concept que les surfaces de Bézier.
Un triangle de Bézier est un cas particulier des surfaces, calculé en interpolant
des points de contrôle. L'interpolation peut être linéaire, quadratique, cubique, etc.
Chaque point de la surface du patch est donc défini comme une pondération des points de contrôle. \\

De manière générale, une courbe de Bézier de degré $n$ possède $(n+1)(n+2) / 2$
points de contrôle $P_{ijk}$ où $(i,j,k) \in \mathbb{N}^3$ sont tels que $i+j+k=n$.
Alors, $\forall (s, t, u) \in \mathbb{R_+}^3$ tels que $s+t+u = 1$, on définit le point de la surface comme :

\[
	S(s,t,u) = \sum_{\begin{smallmatrix} i+j+k=n \\
	i,j,k \ge 0\end{smallmatrix}} {n \choose i\ j\ k } s^i t^j u^k P_{ijk} = \sum_{\begin{smallmatrix} i+j+k=n \\
	i,j,k \ge 0\end{smallmatrix}} \frac{n!}{i!j!k!} s^i t^j u^k P_{ijk} 
\]

\subsection{Fonctionnement de l'algorithme}

L'algorithme fonctionne de la manière suivante :
\begin{enumerate}
\item On définit une fonction auxiliaire qui évalue le polynôme de Bernstein de dimension deux :
\[
	B_{ijk}^n(s,t,u) = {n \choose i\ j\ k } s^i t^j u^k = B_{ij}^n(s,t) = {n \choose i\ j\ k } s^i t^j (1-s-t)^{n-i-j}
\]
\item Cette fonction auxilaire est utilisée dans une fonction qui calcule un seul point de la surface,
en utilisant l'expression de $S(s,t,u)$.
\item Enfin, on fait varier $s \in [0,1]$ et $t \in [0,1-s]$ pour obtenir l'ensemble des points voulus (plus le pas est
fin, plus il y a aura de triangles).
\end{enumerate}

\subsection{Avantages et limitations}

L'utilisation du patch triangulaire est pratique dans les cas où un maillage quadrangulaire est difficile (cas de
la sphère par exemple). \\

Cependant, on peut relever plusieurs limites au modèle. Tout d'abord, pour obtenir un maillage plus lisse il faut
augmenter le degré de la surface ce qui nécessite d'avoir plus de points de contrôle. De plus, la complexité
de l'algorithme est un inconvénient.


\subsection{Conclusion}

Ce modèle peut être utilisé pour lisser des objets 3D représentés par des triangles.
Grâce aux normales des sommets, l'algorithme peut être adapté pour lisser chaque triangle
indépendamment de ses voisins.




\pagebreak

\section{Modèle particulaire}

\subsection{Modèle}

Le modèle particulaire fonctionne à partir d'un ensemble d'objets élémentaires, appelés particules,
soumis à différentes lois (mécanique du point, des fluides...). Le résultat visuel est obtenu
grâce à la superposition de ces particules. \\

Selon l'effet voulu et les lois utilisées, le moteur peut prendre en compte différents paramètres
pour chaque particule :
\begin{itemize}
\item la position, la vitesse et l'accélération ;
\item les forces exercées ;
\item la masse, la charge électrique ;
\item la durée de vie ;
\item etc.
\end{itemize}

De plus, l'apparence peut être définie par une couleur, une opacité et une texture par exemple.

\subsection{Fonctionnement de l'algorithme}

Le programme a été réalisé en C++ avec OpenGL. Le moteur utilise les lois de Newton
relatives à la mécanique du point. Le code est construit principalement autour de deux classes.
La classe \textit{Engine} d'une part, qui gère les entrées de l'utilisateur, affiche les particules
à l'écran et intègre les lois de la dynamique. L'interface \textit{Emitter} d'autre part, qui contrôle
la création et la mise-à-jour des paramètres (autres que vitesse et position). \\
Pour simplifier, les particules sont représentées par des carrés en 2D qui font toujours face à
l'observateur. \\
A chaque étape élémentaire de durée $dt$, on réalise les opérations suivantes :
\begin{enumerate}
\item Ajouter (éventuellement) de nouvelles particules.
\item Mettre à jour les particules (couleur, accélération, notamment).
\item Mettre à jour la vitesse et la position et tout afficher.
\item Supprimer les particules obsolètes.
\end{enumerate}

\subsection{Avantages et limitations}

Ce type de système permet de restituer à l'écran des phénomènes physiques difficiles
à simuler autrement (fumée, feu, eau, nuage, explosion...). \\ 
Cependant, les effets
sont souvent plus réalistes lorsque le nombre de particules augmente, ce qui peut
diminuer les performances du programme. De plus, il faut trouver et implémenter
une modélisation physique adéquate à appliquer aux particules.

\subsection{Conclusion}

Le rendu peut être largement amélioré en utilisant des techniques avancées comme
par exemple les \textit{shaders} OpenGL (pour animer les textures) ou d'autres lois
de la mécanique. L'utilisation d'un très grand nombre de particules nécessite une
étude plus approfondie de la gestion de la mémoire et de l'optimisation des algorithmes.

\pagebreak

\section{Images}

\begin{center}
	\includegraphics[width=16cm]{patches.png}
\end{center}

\begin{center}
	\includegraphics[width=7cm]{explosions.png}
	\includegraphics[width=7cm]{smoke.png}
\end{center}

\end{document}
