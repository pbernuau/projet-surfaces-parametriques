Surfaces paramétriques : Projet 2012 - 2013
=====================

- - - 

Présentation
-------------------------------

Ce projet a été réalisé pendant notre deuxième année d'Informatique et Mathématiques appliquées
à l'ENSEEIHT[^n7] dans le cadre du cours sur les surfaces paramétriques. 

L'objectif du projet était d'illustrer différentes techniques utilisables pour la génération et le
rendu de surfaces ou de volumes.

[**Première partie (Matlab)**](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/src/master/Patches/) :

- Surfaces de Bézier / B-splines uniformes
- Carreaux de Coons
- Surfaces à patches triangulaires

![patches](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/raw/master/Rapport/patches.png)

[**Deuxième partie (C++)**](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/src/master/Particules/) :

- Modèles particulaires (fumée, explosions, etc.)

![explosions](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/raw/master/Rapport/explosions.png)

![smoke](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/raw/master/Rapport/smoke.png)

**Langages et bibliothèques utilisées** :

- Matlab
- C++
- OpenGL
- GLUT

**Liens complémentaires** :

- [Sujet du projet (PDF)](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/downloads/Sujet2013.pdf)
- [Rapport (PDF)](https://bitbucket.org/pbernuau/projet-surfaces-parametriques/downloads/Rapport.pdf)

Compilation et exécution
--------------------------------------

Compilation (nécessite *make* et *freeglut*) :
```
#!bash
cd Particules
make -f scripts/Makefile
```

Pour démarrer le programme :
```
#!bash
./bin/app
```

Les déplacements se font au clavier (`Z, S, Q, D`) et à la souris. Un menu contextuel permet de
choisir une animation.


[^n7]: [École nationale supérieure d'électrotechnique, d'électronique, d'informatique, d'hydraulique
et des télécommunications](http://www.enseeiht.fr/fr/index.html)